package com.bank.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class AccountBankApiApplication extends SpringBootServletInitializer {

	//public static void main(String[] args) {
	//	SpringApplication.run(AccountBankApiApplication.class, args);
	//}

	public static void main(String[] args)
	{
		new AccountBankApiApplication().configure(new SpringApplicationBuilder(AccountBankApiApplication.class)).run(args);
	}

}
