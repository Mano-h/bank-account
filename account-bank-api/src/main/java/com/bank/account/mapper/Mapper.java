package com.bank.account.mapper;

import com.bank.account.entity.Account;
import com.bank.account.entity.Client;
import com.bank.account.model.AccountModel;
import com.bank.account.model.ClientModel;



public class Mapper {

    public static Client mapFromModel(ClientModel clientModel){
        Client client = new Client();
        client.setName(clientModel.getName());
        client.setFirstname(clientModel.getFirstname());
        client.setBirtDayDate(clientModel.getBirtDayDate());
        if (clientModel.getId()!=null){
            client.setIdClient(clientModel.getId());
        }
       return  client;
    }

    public static Account mapFromModel(AccountModel accountModel){
      Account account = new Account();
      account.setAccountNumber(accountModel.getAccountNumber());
      account.setAccountType(accountModel.getAccountType());
      account.setBalance(accountModel.getBalance());
      account.setDomiciliation(accountModel.getDomiciliation());
      account.setIban(accountModel.getIban());
      account.setRib(accountModel.getRib());
      return  account;
    }





}
