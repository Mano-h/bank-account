package com.bank.account.controler.account;

import com.bank.account.entity.Account;
import com.bank.account.mapper.Mapper;
import com.bank.account.model.AccountModel;
import com.bank.account.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/account")
public class AccountControler {

    @Autowired
    private AccountRepository accountRepository;

    @PostMapping
    public ResponseEntity<Account> save(@RequestBody AccountModel accountModel){
     return  ResponseEntity.ok(accountRepository.save(Mapper.mapFromModel(accountModel)));
    }
}
