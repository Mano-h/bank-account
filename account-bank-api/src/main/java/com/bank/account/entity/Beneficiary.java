package com.bank.account.entity;

import javax.persistence.*;

@Entity
public class Beneficiary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idBeneficiary;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "firstname" ,nullable = false)
    private String firstname;
    @Column(name = "accountNumber")
    private String accountNumber;
    @Column(name = "rib" ,unique = true, nullable = false)
    private String rib;
}
