package com.bank.account.entity;

import lombok.Builder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "CLIENTS")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idClient;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "firstname" ,nullable = false)
    private String firstname;
    @Column(name = "birtDayDate")
    private LocalDate birtDayDate;

    public Client() {
    }

    public Long getIdClient() {
        return idClient;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public LocalDate getBirtDayDate() {
        return birtDayDate;
    }

    public void setBirtDayDate(LocalDate birtDayDate) {
        this.birtDayDate = birtDayDate;
    }
}
