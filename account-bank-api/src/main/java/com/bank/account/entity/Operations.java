package com.bank.account.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "OPERATIONS")
public class Operations implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idOperation;
    @Column(name = "AMOUNT_OPERATION")
    private Double amountOperation;
    @Column(name = "DATE_OPERATION")
    private LocalDate dateOperation;
    @OneToOne
    private Beneficiary beneficiary;
    @OneToOne
    private Account account;

    public Operations() {
    }

    public Long getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(Long idOperation) {
        this.idOperation = idOperation;
    }

    public Double getAmountOperation() {
        return amountOperation;
    }

    public void setAmountOperation(Double amountOperation) {
        this.amountOperation = amountOperation;
    }

    public LocalDate getDateOperation() {
        return dateOperation;
    }

    public void setDateOperation(LocalDate dateOperation) {
        this.dateOperation = dateOperation;
    }

    public Beneficiary getBeneficiary() {return beneficiary;}

    public void setBeneficiary(Beneficiary beneficiary) {this.beneficiary = beneficiary;}

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
