package com.bank.account.data;

import com.bank.account.entity.Client;
import com.bank.account.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import java.time.LocalDate;

@Singleton
@Configuration
public class initData {

    @Autowired
    private ClientRepository clientRepository;

    @PostConstruct
    private void initdata(){
        System.out.println("*******************Initialisation des données *********************** 1");
        // Create Client
        Client client1 = new Client();
        client1.setBirtDayDate(LocalDate.of(1986,05,20));
        client1.setFirstname("Hermann");
        client1.setName("YEMDJEU");

        System.out.println("*******************Initialisation des données *********************** 2");

        clientRepository.save(client1);


    }
}
