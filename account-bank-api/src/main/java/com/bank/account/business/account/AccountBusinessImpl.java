package com.bank.account.business.account;

import com.bank.account.entity.Account;
import com.bank.account.mapper.Mapper;
import com.bank.account.model.AccountModel;
import com.bank.account.repository.AccountRepository;
import com.bank.account.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AccountBusinessImpl implements AccountBusiness {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private ClientRepository  clientRepository;

    @Override
    public Account save(AccountModel accountModel) {
        Account account = Mapper.mapFromModel(accountModel);
        account.setClients(clientRepository.getById(accountModel.getIdClient()));
        return accountRepository.save(Mapper.mapFromModel(accountModel));
    }

    @Override
    public Account updateClient(AccountModel accountModel) {
        return null;
    }

    @Override
    public Account findAccountByNumber(String accountNumber) {
        return null;
    }

    @Override
    public List<Account> findAllAccount() {
        return null;
    }
}
