package com.bank.account.model;


import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientModel {
    private Long id;
    private String name;
    private String firstname;
    private LocalDate birtDayDate;
}
